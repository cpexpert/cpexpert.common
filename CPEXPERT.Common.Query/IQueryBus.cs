﻿using System.Threading.Tasks;

namespace CPEXPERT.Common.Query
{
	public interface IQueryBus
	{
		Task<TResponse> Execute<TQueryData, TResponse>(Query<TQueryData, TResponse> query);
	}
}