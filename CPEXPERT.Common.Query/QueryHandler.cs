﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Serilog;

namespace CPEXPERT.Common.Query
{
	public abstract class QueryHandler<TQueryData, TResponse> : IRequestHandler<Query<TQueryData, TResponse>, TResponse>
	{
		public readonly ILogger Logger;
		protected QueryHandler(ILogger logger)
		{
			Logger = logger;
		}

		public abstract Task<TResponse> Handle(Query<TQueryData, TResponse> query, CancellationToken cancellationToken);
	}
}