﻿using System;
using MediatR;

namespace CPEXPERT.Common.Query
{
	public class Query<TQueryData, TResponse> : IRequest<TResponse>
	{
		public Query()
		{
			Id = Guid.NewGuid();
			Created = DateTime.UtcNow;
			RequestId = Guid.Empty;
			RequestCreated = DateTime.MinValue;
			CanCache = false;
			QueryData = default;
		}

		public Guid Id { get; }
		public DateTime Created { get; }
		public Guid RequestId { get; set; }
		public DateTime RequestCreated { get; set; }
		public bool CanCache { get; set; }
		public TQueryData QueryData { get; set; }
	}
}