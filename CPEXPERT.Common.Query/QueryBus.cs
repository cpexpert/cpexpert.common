﻿using System.Threading.Tasks;
using MediatR;

namespace CPEXPERT.Common.Query
{
	public class QueryBus : IQueryBus
	{
		private readonly IMediator _mediator;
		public QueryBus(IMediator mediator)
		{
			_mediator = mediator;
		}

		public Task<TResponse> Execute<TQueryData, TResponse>(Query<TQueryData, TResponse> query)
		{
			return _mediator.Send(query);
		}
	}
}