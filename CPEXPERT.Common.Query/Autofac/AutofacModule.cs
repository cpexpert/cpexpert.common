﻿using Autofac;
using Serilog;

namespace CPEXPERT.Common.Query.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<QueryBus>().As<IQueryBus>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.Common.Query.Autofac].Load(...)");
		}
	}
}
