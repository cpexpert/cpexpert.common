﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Filters;

namespace CPEXPERT.Common.DAL.Interfaces
{
	public interface IBaseRepositoryAsync<TEntity, TDbKeyType> : IDisposable where TEntity : BaseEntity<TDbKeyType>,  new()
	{
		Task<TDbKeyType> Add(TEntity entity);
		Task<ICollection<TDbKeyType>> AddRange(ICollection<TEntity> entities);
		Task<IDictionary<TDbKeyType, bool>> DeleteRange(ICollection<TDbKeyType> ids);
		Task<bool> Delete(Guid appKey);
		Task<IDictionary<Guid, bool>> DeleteRange(ICollection<Guid> appKeys);
		Task<bool> Update(TEntity entity);
		Task<IDictionary<TDbKeyType, bool>> UpdateRange(ICollection<TEntity> entities);
		Task<TResponseData> GetByAppKey<TResponseData>(Guid appKey) where TResponseData : class;
		Task<TEntity> GetByAppKey(Guid appKey);
		Task<PaginatedListResponseData<TResponseData>> GetFiltered<TResponseData>(Expression<Func<TEntity, bool>> expression = null, QueryFilterRequest filterRequest = null) where TResponseData : class;
		Task<TResponseData> SingleOrDefault<TResponseData>(Expression<Func<TEntity, bool>> expression) where TResponseData : class;
		Task<TEntity> SingleOrDefault(Expression<Func<TEntity, bool>> expression);
		Task<TResponseData> FirstOrDefault<TResponseData>(Expression<Func<TEntity, bool>> expression) where TResponseData : class;
		Task<TEntity> FirstOrDefault(Expression<Func<TEntity, bool>> expression);
	}
}