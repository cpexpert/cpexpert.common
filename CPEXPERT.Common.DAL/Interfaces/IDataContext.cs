﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Domain.Enums;

namespace CPEXPERT.Common.DAL.Interfaces
{
	public interface IDataContext
	{
		bool Migrate();
		bool Seed();
		int SaveChanges(Guid contextUserId);
		Task<int> SaveChangesAsync(Guid contextUserId, CancellationToken cancellationToken);
		DataBaseTypes DbType { get; }
		bool? ForcedExecuteSeed { get; }
	}
}