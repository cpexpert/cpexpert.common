﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.DAL.SqlLite.Configurations;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Domain.Enums;
using CPEXPERT.Common.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Serilog;

namespace  CPEXPERT.Common.DAL.SqlLite
{
  public abstract class DataBaseContext : DbContext, IDataContext
  {
	  private readonly ILogger _logger;

	  protected DataBaseContext(ILogger logger)
	  {
		  _logger = logger;
		  _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].constructor...");
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
	    _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].OnConfiguring");
      var connectionString = ConfigurationHelper.GetConfigSection<SqlLiteConnectionSettings>("Connection").ConnectionString;
      optionsBuilder.UseSqlite(connectionString);
    }

    public bool Migrate()
    {
	    _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].Migrate");
      try
      {
	      _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].Migrate.connectionString={0}", Database.GetDbConnection().ConnectionString);
        Database.Migrate();
	      return true;
      }
      catch (Exception e)
      {
	      _logger.Error(e, "Migrate SqlLite Data base ERROR");
        throw;
      }
    }

    public override int SaveChanges()
    {
	    return SaveChanges(Statics.Users.TechSystemUser.Id);
    }

    public int SaveChanges(Guid contextUserId)
    {
	    _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].SaveChanges");
      foreach (var entry in ChangeTracker.Entries<BaseEntity<long>>().Where(c => c.State != EntityState.Unchanged && c.State != EntityState.Detached).Where(entry => entry.Entity != null))
	    {
		    ExtendedActionBeforeSaveOnEntity(entry, contextUserId);
	    }

	    return base.SaveChanges();
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
	    return SaveChangesAsync(Statics.Users.TechSystemUser.Id, cancellationToken);
    }

    public Task<int> SaveChangesAsync(Guid contextUserId, CancellationToken cancellationToken = new CancellationToken())
    {
	    _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].SaveChangesAsync");
      foreach (var entry in ChangeTracker.Entries<BaseEntity<long>>().Where(c => c.State != EntityState.Unchanged && c.State != EntityState.Detached).Where(entry => entry.Entity != null))
	    {
		    ExtendedActionBeforeSaveOnEntity(entry, contextUserId);
	    }

	    return base.SaveChangesAsync(cancellationToken);
    }

    public void ExtendedActionBeforeSaveOnEntity(EntityEntry<BaseEntity<long>> entry, Guid contextUserId)
    {
	    _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].ExtendedActionBeforeSaveOnEntity");
      if (entry?.Entity != null)
      {
        var dateTimeUtc = DateTime.UtcNow;
        var mustDefineNewGuid = entry.Entity.AppKey == Guid.Empty;

        switch (entry.State)
        {
          case EntityState.Added:
            if (mustDefineNewGuid)
            {
              entry.Entity.AppKey = Guid.NewGuid();
            }
            entry.Entity.CreateDateUtc = dateTimeUtc;
            entry.Entity.LastEditDateUtc = dateTimeUtc;
            entry.Entity.LastEditUser = contextUserId;
            entry.Entity.CreateUser = contextUserId;

            break;
          case EntityState.Modified:
            entry.Entity.LastEditDateUtc = dateTimeUtc;
            entry.Entity.LastEditUser = contextUserId;
            break;
        }
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
	    _logger.Debug("[CPEXPERT.Common.DAL.SqlLite.DataBaseContext].OnModelCreating");
      base.OnModelCreating(modelBuilder);
    }

    public DataBaseTypes DbType => DataBaseTypes.SqlLite;
    public abstract bool Seed();
    public bool? ForcedExecuteSeed { get; private set; }
  }
}