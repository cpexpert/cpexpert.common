﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Filters;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace CPEXPERT.Common.DAL.SqlLite.Repositories
{
  //create per request/ task
  public class BaseRepositoryAsync<TEntity> : IBaseRepositoryAsync<TEntity, Guid, long> where TEntity : BaseEntity<long>, new()
  {
    protected readonly DataBaseContext _context;
    protected readonly DbSet<TEntity> _dbSet;
    protected readonly ILogger _logger;

    protected BaseRepositoryAsync(DataBaseContext context, ILogger logger)
    {
      _context = context;
      _dbSet = _context.Set<TEntity>();
      _logger = logger;
    }

    public virtual void Dispose()
    {
      _context?.Dispose();
    }

    public virtual async Task<long> Add(TEntity entity, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();

      try
      {
        _dbSet.Add(entity);
        await _context.SaveChangesAsync(userContext);
        return entity.DbKey;
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Add entity to DB total time: {0} [ms]", stopper.Elapsed.TotalMilliseconds);
      }
    }

    public virtual async Task<ICollection<long>> AddRange(ICollection<TEntity> entities, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();
      try
      {
        _dbSet.AddRange(entities);
        await _context.SaveChangesAsync(userContext);
        return entities.Where(x => x.DbKey > 0 && x.AppKey != Guid.Empty).Select(x => x.DbKey).ToList();
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Add {0} entities on DB total time: {1} [ms]", entities.Count, stopper.Elapsed.TotalMilliseconds);
      }
    }

    public virtual async Task<bool> Delete(long id, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();

      try
      {
        var entity = await _dbSet.AsNoTracking().SingleOrDefaultAsync(e => e.DbKey == id);
        if (entity != null)
        {
          _dbSet.Remove(entity);
          await _context.SaveChangesAsync(userContext);
          return _dbSet.AsNoTracking().SingleOrDefault(e => e.DbKey == id) == null;
        }

        return false;
      }
      finally
      {
	      stopper.Stop();
        _logger.Debug("Delete entity to DB total time: {0} [ms]", stopper.Elapsed.TotalMilliseconds);
      }
    }

    public virtual async Task<IDictionary<long, bool>> DeleteRange(ICollection<long> ids, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();
      try
      {
        var returnDictionary = new Dictionary<long, bool>();

        var entities = _dbSet.AsNoTracking().Where(x => ids.Contains(x.DbKey));
        if (ids.Count == entities.Count())
        {
          _dbSet.RemoveRange(entities);
          await _context.SaveChangesAsync(userContext);

          foreach (var id in ids)
          {
            returnDictionary.Add(id, _dbSet.AsNoTracking().SingleOrDefault(x => x.DbKey == id) == null);
          }
        }

        return returnDictionary;
      }
      finally
      {
	      stopper.Stop();
        _logger.Debug("Delete {0} entities on DB total time: {1} [ms]", ids.Count, stopper.Elapsed.TotalMilliseconds);
      }
    }

    public virtual async Task<bool> Delete(Guid appKey, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();

      try
      {
        var entity = await _dbSet.AsNoTracking().SingleOrDefaultAsync(e => e.AppKey == appKey);
        if (entity != null)
        {
          _dbSet.Remove(entity);
          await _context.SaveChangesAsync(userContext);
          return _dbSet.AsNoTracking().SingleOrDefault(e => e.AppKey == appKey) == null;
        }

        return false;
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Delete entity to DB total time: {0} [ms]", stopper.Elapsed.TotalMilliseconds);
      }
    }

    public virtual async Task<IDictionary<Guid, bool>> DeleteRange(ICollection<Guid> appKeys, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();
      try
      {
        var returnDictionary = new Dictionary<Guid, bool>();

        var entities = _dbSet.AsNoTracking().Where(x => appKeys.Contains(x.AppKey));
        if (appKeys.Count == entities.Count())
        {
          _dbSet.RemoveRange(entities);
          await _context.SaveChangesAsync(userContext);

          foreach (var id in appKeys)
          {
            returnDictionary.Add(id, _dbSet.AsNoTracking().SingleOrDefault(x => x.AppKey == id) == null);
          }
        }

        return returnDictionary;
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Delete {0} entities on DB total time: {1} [ms]", appKeys.Count, stopper.Elapsed.TotalMilliseconds);
      }
    }

    public virtual async Task<bool> Update(TEntity entity, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();

      try
      {
        _context.Entry(entity).State = EntityState.Modified;
        await _context.SaveChangesAsync(userContext);
        _context.Entry(entity).State = EntityState.Detached;
        return true;
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Update entity to DB total time: {0} [ms]", stopper.Elapsed.TotalMilliseconds);
      }
    }

    public virtual async Task<IDictionary<long, bool>> UpdateRange(ICollection<TEntity> entities, Guid userContext)
    {
      var stopper = new Stopwatch();
      stopper.Start();
      try
      {
        var returnDictionary = new Dictionary<long, bool>();

        foreach (var entity in entities)
        {
          _context.Entry(entity).State = EntityState.Modified;
        }

        await _context.SaveChangesAsync(userContext);

        foreach (var entity in entities)
        {
          _context.Entry(entity).State = EntityState.Detached;
          returnDictionary.Add(entity.DbKey, true);
        }

        return returnDictionary;
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Save {0} entities on DB total time: {1} [ms]", entities.Count, stopper.Elapsed.TotalMilliseconds);
      }
    }

    public Task<TResponseData> GetByAppKey<TResponseData>(Guid appKey) where TResponseData : class
    {
	    throw new NotImplementedException();
    }

    public Task<PaginatedListResponseData<TResponseData, TEntity, long>> GetFiltered<TResponseData>(Expression<Func<TEntity, bool>> expression = null, QueryFilterRequest filterRequest = null) where TResponseData : class
    {
	    throw new NotImplementedException();
    }

    public Task<TResponseData> SingleOrDefault<TResponseData>(Expression<Func<TEntity, bool>> expression) where TResponseData : class
    {
	    throw new NotImplementedException();
    }

    public Task<TResponseData> FirstOrDefault<TResponseData>(Expression<Func<TEntity, bool>> expression) where TResponseData : class
    {
	    throw new NotImplementedException();
    }
  }
}