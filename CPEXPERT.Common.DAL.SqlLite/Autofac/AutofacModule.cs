﻿using Autofac;
using Serilog;

namespace CPEXPERT.Common.DAL.SqlLite.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			Log.Debug("[CPEXPERT.Common.DAL.SqlLite.Autofac].Load(...)");
		}
	}
}
