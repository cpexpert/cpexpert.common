﻿namespace CPEXPERT.Common.DAL.SqlLite.Configurations
{
	public class SqlLiteConnectionSettings
	{
		public string ConnectionString { get; set; }
	}
}