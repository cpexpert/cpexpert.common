﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Domain.Enums;

namespace CPEXPERT.Common.DAL.MySql
{
  public abstract class DataBaseContext : IDataContext
  {
	  protected DataBaseContext(bool? forcedExecuteSeed)
	  {
		  ForcedExecuteSeed = forcedExecuteSeed;
	  }

	  public bool Migrate()
	  {
		  throw new System.NotImplementedException();
	  }

	  public int SaveChanges(Guid contextUserId)
	  {
		  throw new NotImplementedException();
	  }

	  public Task<int> SaveChangesAsync(Guid contextUserId, CancellationToken cancellationToken)
	  {
		  throw new NotImplementedException();
	  }

	  public DataBaseTypes DbType => DataBaseTypes.MySql;
	  public abstract bool Seed();
		public bool? ForcedExecuteSeed { get; private set; }
  }
}