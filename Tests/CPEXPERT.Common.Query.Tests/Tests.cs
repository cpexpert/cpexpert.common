﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Test.DbStore;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPEXPERT.Common.Query.Tests
{
	[TestClass]
	public class Tests
	{
		private TestDataStore _testDataStore { get; set; }
		private IContainer _autofacContainer;
		private IQueryBus _queryBus;
		protected IContainer AutofacContainer
		{
			get
			{
				if (_autofacContainer == null)
				{
					var builder = new ContainerBuilder();
					var appAssemblies = new List<Assembly>(Directory
						.EnumerateFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location), "*.dll",
							SearchOption.AllDirectories)
						.Where(fullFilePath => Path.GetFileName(fullFilePath).StartsWith("CPEXPERT.")).ToList()
						.Select(Assembly.LoadFrom));
					appAssemblies.ForEach(assembly => builder.RegisterAssemblyModules(assembly));
					builder.AddAutoMapper(appAssemblies.ToArray());
					builder.AddMediatR(appAssemblies);
					var container = builder.Build();

					_autofacContainer = container;
				}

				return _autofacContainer;
			}
		}

		[TestInitialize]
		public void TestInitialize()
		{
			_queryBus = AutofacContainer.Resolve<IQueryBus>();
			_testDataStore = AutofacContainer.Resolve<TestDataStore>();
		}

		[TestMethod]
		public async Task ExecuteQuerySuccess()
		{
			// arrange
			// act
			var query = new TestQuerySingleOrDefault
			{
				CanCache = false,
				RequestCreated = DateTime.UtcNow,
				RequestId = Guid.NewGuid(),
				QueryData = _testDataStore.Entities.FirstOrDefault()?.AppKey ?? Guid.Empty
			};
			var queryResult = await _queryBus.Execute(query);

			// assert
			Assert.IsNotNull(_testDataStore);
			Assert.IsNotNull(_testDataStore.Entities);
			Assert.IsTrue(_testDataStore.Entities.Count == 1000);
			Assert.IsNotNull(queryResult);
			Assert.IsTrue(queryResult.IsSuccess);
			Assert.IsTrue(queryResult.RequestId == query.RequestId);
			Assert.IsTrue(queryResult.RequestCreated == query.RequestCreated);
			Assert.IsTrue(queryResult.CanCache == query.CanCache);
			Assert.IsNotNull(queryResult.Messages);
			Assert.IsTrue(queryResult.Messages.FirstOrDefault(x=>x.Type == MessageTypes.Error) == null);
		}

		[TestMethod]
		public async Task ExecuteQueryError()
		{
			// arrange
			var appKey = _testDataStore.Entities.FirstOrDefault()?.AppKey ?? Guid.Empty;
			_testDataStore.Entities.Add(new FakeEntity
			{
				AppKey = appKey,
				Name = "duplicate",
				Number = 2222
			});
			// act
			var query = new TestQuerySingleOrDefault
			{
				CanCache = false,
				RequestCreated = DateTime.UtcNow,
				RequestId = Guid.NewGuid(),
				QueryData = appKey
			};
			var queryResult = await _queryBus.Execute(query);

			_testDataStore.Entities.Remove(_testDataStore.Entities.LastOrDefault(x => x.AppKey == appKey));

			// assert
			Assert.IsNotNull(_testDataStore);
			Assert.IsNotNull(_testDataStore.Entities);
			Assert.IsTrue(_testDataStore.Entities.Count == 1000);
			Assert.IsNotNull(queryResult);
			Assert.IsFalse(queryResult.IsSuccess);
			Assert.IsTrue(queryResult.RequestId == query.RequestId);
			Assert.IsTrue(queryResult.RequestCreated == query.RequestCreated);
			Assert.IsTrue(queryResult.CanCache == query.CanCache);
			Assert.IsNotNull(queryResult.Messages);
			Assert.IsTrue(queryResult.Messages.FirstOrDefault(x => x.Type == MessageTypes.Error) != null);
		}
	}
}