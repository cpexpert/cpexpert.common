﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Test.DbStore;
using CPEXPERT.Common.Test.ResponseData;
using MediatR;

namespace CPEXPERT.Common.Query.Tests
{
	public class TestQuerySingleOrDefaultHandler : IRequestHandler<TestQuerySingleOrDefault, Response<Guid>>
	{

		private readonly IBaseRepositoryAsync<FakeEntity, long> _repository;

		public TestQuerySingleOrDefaultHandler(IBaseRepositoryAsync<FakeEntity, long> repository)
		{
			_repository = repository;
		}

		public Task<Response<Guid>> Handle(TestQuerySingleOrDefault query, CancellationToken cancellationToken)
		{
			var response = new Response<Guid>
			{
				CanCache = query.CanCache,
				RequestCreated = query.RequestCreated,
				RequestId = query.RequestId
			};

			try
			{
				var entity = (_repository.SingleOrDefault<FakeResponseData>(x => x.AppKey == query.QueryData)).Result;
				response.ResponseData = entity?.AppKey ?? Guid.Empty;
			}
			catch (Exception e)
			{
				response.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
			}

			return Task.FromResult(response);
		}
	}
}
