﻿using System;

namespace CPEXPERT.Common.Command.Tests
{
    public class ItemAddRequestData
    {
        public Guid AppKey { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
