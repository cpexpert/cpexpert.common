﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using CPEXPERT.Common.Test.DbStore;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPEXPERT.Common.Command.Tests
{
	[TestClass]
	public class Tests
	{
		private TestDataStore _testDataStore { get; set; }
		private ICommandBus _commandBus { get; set; }
		private IContainer _autofacContainer;

		protected IContainer AutofacContainer
		{
			get
			{
				if (_autofacContainer == null)
				{
					var builder = new ContainerBuilder();
					var appAssemblies = new List<Assembly>(Directory
						.EnumerateFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location), "*.dll",
							SearchOption.AllDirectories)
						.Where(fullFilePath => Path.GetFileName(fullFilePath).StartsWith("CPEXPERT.")).ToList()
						.Select(Assembly.LoadFrom));
					appAssemblies.ForEach(assembly => builder.RegisterAssemblyModules(assembly));
					builder.AddAutoMapper(appAssemblies.ToArray());
					builder.AddMediatR(appAssemblies);
					var container = builder.Build();

					_autofacContainer = container;
				}

				return _autofacContainer;
			}
		}

		[TestInitialize]
		public void TestInitialize()
		{
			_commandBus = AutofacContainer.Resolve<ICommandBus>();
			_testDataStore = AutofacContainer.Resolve<TestDataStore>();
		}


		[TestMethod]
		public async Task ValidateCommandSuccess()
		{
			// arrange
			var guid = Guid.NewGuid();
			var number = new Random().Next(int.MinValue, int.MaxValue);
			var command = new TestCommandAddItem
			{
				CommandData = new ItemAddRequestData
				{
					AppKey = guid,
					Name = "NEW_ITEM_ADD_" + guid.ToString().Replace("-", ""),
					Number = number
				}
			};
			// act
			var validateResult = await _commandBus.ValidateAndPublish(command);
			// assert
			Assert.IsNotNull(validateResult);
		}

		[TestMethod]
		public async Task ExecuteCommandSuccess()
		{
			// arrange
			var appKey = Guid.NewGuid();
			var number = new Random().Next(int.MinValue, int.MaxValue);
			var command = new TestCommandAddItem
			{
				CommandData = new ItemAddRequestData
				{
					AppKey = appKey,
					Name = "NEW_ITEM_ADD_" + appKey.ToString().Replace("-", ""),
					Number = number
				}
			};
			// act
			await _commandBus.Execute(command);
			var afterExecuteCommandData = _testDataStore.Entities.SingleOrDefault(s => s.AppKey == appKey);
			// assert
			Assert.IsNotNull(afterExecuteCommandData);
		}
	}
}