﻿using FluentValidation;

namespace CPEXPERT.Common.Command.Tests
{
    public class TestCommandAddItemValidator : AbstractValidator<ItemAddRequestData>
    {
	    public TestCommandAddItemValidator()
	    {
		    RuleFor(item => item)
			    .NotNull()
			    .WithMessage("Element is null");
	    }
    }
}
