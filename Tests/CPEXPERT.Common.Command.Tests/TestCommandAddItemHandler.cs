﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Event;
using CPEXPERT.Common.Service.SignalR;
using CPEXPERT.Common.Test.DbStore;
using FluentValidation;
using Serilog;

namespace CPEXPERT.Common.Command.Tests
{
	public class TestCommandAddItemHandler : CommandHandler<TestCommandAddItem, ItemAddRequestData>
	{

		private readonly IBaseRepositoryAsync<FakeEntity, long> _repository;

		public TestCommandAddItemHandler(IValidatorFactory validatorFactory, ILogger logger,
			ISignalRCommandService signalRCommandService, IBaseRepositoryAsync<FakeEntity, long> repository, IEventBus eventBus, IUserDataProvider userDataProvider) : base(
			validatorFactory, logger, signalRCommandService, eventBus, userDataProvider)
		{
			_repository = repository;
		}

		public override Task<MessageData> Execute(TestCommandAddItem command, CancellationToken cancellationToken)
		{
			if (command?.CommandData == null)
			{
				throw new Exception("Command or Command data is null");
			}

			_repository.Add(new FakeEntity
			{
				AppKey = command.CommandData.AppKey,
				Number = command.CommandData.Number,
				Name = command.CommandData.Name
			});

			return Task.FromResult<MessageData>(new MessageData
			{
				Message = "OK",
				Language = Languages.English,
				Type = MessageTypes.Information
			});
		}

		public override Task StoreCommandOnEventStore(TestCommandAddItem command)
		{
			return Task.CompletedTask;
		}
	}
}
