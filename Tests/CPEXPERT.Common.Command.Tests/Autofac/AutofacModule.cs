﻿using Autofac;
using CPEXPERT.Common.Command.Validators;
using FluentValidation;

namespace CPEXPERT.Common.Command.Tests.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<TestCommandAddItemValidator>().As<IValidator<ItemAddRequestData>>().InstancePerLifetimeScope();
		}
	}
}