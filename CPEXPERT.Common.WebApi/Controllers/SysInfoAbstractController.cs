﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Query;
using CPEXPERT.Common.Structures;
using Microsoft.AspNetCore.Authorization;
using Serilog;

namespace CPEXPERT.Common.WebApi.Controllers
{
	public abstract class SysInfoAbstractController : BaseController
	{
		protected SysInfoAbstractController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper) : base(queryBus, commandBus, logger, mapper)
		{
		}

		[HttpGet("Version")]
		[AllowAnonymous]
		public string Version()
		{
			var assembly = Assembly.GetEntryAssembly();
			if (assembly == null) return "Error on grab information about WebAPI";
			var version = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion;
			var copyright = assembly.GetCustomAttributes<AssemblyCopyrightAttribute>().FirstOrDefault()?.Copyright;
			var product = assembly.GetCustomAttributes<AssemblyProductAttribute>().FirstOrDefault()?.Product;

			return $"Date: {DateTime.UtcNow:g}{Environment.NewLine}" +
			       $"{copyright}{Environment.NewLine}" +
			       $"Name: {product}{Environment.NewLine}" +
			       $"Version: {version}";

		}

		[HttpGet("GetModuleInformation")]
		public abstract Task<ModuleInfo> GetModuleInformation();
	}
}