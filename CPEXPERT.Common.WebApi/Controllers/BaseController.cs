﻿using AutoMapper;
using CPEXPERT.Common.Command;
using CPEXPERT.Common.Query;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace CPEXPERT.Common.WebApi.Controllers
{
	public class BaseController : ControllerBase
	{
		protected readonly IQueryBus _queryBus;
		protected readonly ICommandBus _commandBus;
		protected readonly ILogger _logger;
		protected readonly IMapper _mapper;

		public BaseController(IQueryBus queryBus, ICommandBus commandBus, ILogger logger, IMapper mapper)
		{
			_queryBus = queryBus;
			_commandBus = commandBus;
			_logger = logger;
			_mapper = mapper;
		}
	}
}