﻿using Autofac;
using CPEXPERT.Common.Abstracts;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace CPEXPERT.Common.WebApi.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().SingleInstance();
			builder.RegisterType<WebApiUserDataProvider>().As<IUserDataProvider>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.Common.WebApi.Autofac].Load(...)");
		}
	}
}
