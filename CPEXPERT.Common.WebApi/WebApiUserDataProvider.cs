﻿using System;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.Helpers;
using Microsoft.AspNetCore.Http;

namespace CPEXPERT.Common.WebApi
{
	public class WebApiUserDataProvider : IUserDataProvider
	{
		private readonly IHttpContextAccessor _httpContextAccessor;
		public WebApiUserDataProvider(IHttpContextAccessor httpContextAccessor = null)
		{
			_httpContextAccessor = httpContextAccessor;
		}

		public virtual Guid GetCurrentUserAppKey()
		{
			return _httpContextAccessor?.HttpContext == null ? Guid.Empty : AuthHelper.GetCurrentUserAppKey(_httpContextAccessor.HttpContext);
		}
	}
}