﻿namespace CPEXPERT.Common.DAL.MsSql.Configurations
{
	public class MsSqlConnectionSettings
	{
		public string ConnectionString { get; set; }
		public bool? ForcedExecuteSeed { get; set; }
	}
}