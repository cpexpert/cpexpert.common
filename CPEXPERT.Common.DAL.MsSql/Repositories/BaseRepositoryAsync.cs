﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Domain.Entities;
using CPEXPERT.Common.Filters;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace CPEXPERT.Common.DAL.MsSql.Repositories
{
  //create per request/ task
  public class BaseRepositoryAsync<TEntity> : IBaseRepositoryAsync<TEntity, long> where TEntity : BaseEntity<long>, new()
  {
	  private readonly DataBaseContext _context;
	  private readonly DbSet<TEntity> _dbSet;
    private readonly ILogger _logger;
    private readonly string _entityName;
    private readonly IMapper _mapper;
    public readonly Guid UserContextTask;

    protected BaseRepositoryAsync(DataBaseContext context, ILogger logger, IMapper mapper, IUserDataProvider userDataProvider)
    {
      _context = context;
      _dbSet = _context.Set<TEntity>();
      _logger = logger;
      _entityName = typeof(TEntity).Name;
      _mapper = mapper;
      UserContextTask = userDataProvider.GetCurrentUserAppKey();
    }

    public virtual void Dispose()
    {
      _context?.Dispose();
    }

    public virtual async Task<long> Add(TEntity entity)
    {
      var stopper = new Stopwatch();
      stopper.Start();

      try
      {
        if(UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
	      await _dbSet.AddAsync(entity);
	      await _context.SaveChangesAsync(UserContextTask);
	      return entity.DbKey;
      }
      catch(Exception e)
      {
	      _logger.Error(e, "Error when execute Add [{@_entityName}] with data: {@entity} | Context user: {@userContext}", _entityName, entity, UserContextTask);
	      return -1;
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Add [{@_entityName}] to DB total time: {1} [ms] | Context user: {@userContext}", _entityName, stopper.Elapsed.TotalMilliseconds, UserContextTask);
      }
    }

    public virtual async Task<ICollection<long>> AddRange(ICollection<TEntity> entities)
    {
      var stopper = new Stopwatch();
      stopper.Start();
      try
      {
	      if (UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
        await _dbSet.AddRangeAsync(entities);
        await _context.SaveChangesAsync(UserContextTask);
        return entities.Where(x => x.DbKey > 0 && x.AppKey != Guid.Empty).Select(x => x.DbKey).ToList();
      }
      catch (Exception e)
      {
	      _logger.Error(e, "Error when execute AddRange [{@_entityName}] with data: {@entities} | Context user: {@userContext}", _entityName, entities, UserContextTask);
	      return new List<long>();
      }
      finally
      {
        stopper.Stop();
        _logger.Debug("Add {0} [{@_entityName}] on DB total time: {1} [ms] | Context user: {@userContext}", entities.Count, _entityName, stopper.Elapsed.TotalMilliseconds, UserContextTask);
      }
    }

    public virtual async Task<IDictionary<long, bool>> DeleteRange(ICollection<long> ids)
    {
	    if (UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
      var stopper = new Stopwatch();
      stopper.Start();
      var returnDictionary = new Dictionary<long, bool>();
      try
      {
	      if (UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
        var entities = _dbSet.AsNoTracking().Where(x => ids.Contains(x.DbKey));
        if (ids.Count == entities.Count())
        {
          _dbSet.RemoveRange(entities);
          await _context.SaveChangesAsync(UserContextTask);

          foreach (var id in ids)
          {
            returnDictionary.Add(id, _dbSet.AsNoTracking().SingleOrDefault(x => x.DbKey == id) == null);
          }
        }

        return returnDictionary;
      }
      catch (Exception e)
      {
	      _logger.Error(e, "Error when execute DeleteRange [{@_entityName}] deleting list: {@returnDictionary} | Context user: {@userContext}", _entityName, returnDictionary, UserContextTask);
	      return returnDictionary;
      }
      finally
      {
	      stopper.Stop();
        _logger.Debug("Delete {0} [{@_entityName}] on DB total time: {2} [ms] | Context user: {@userContext}", ids.Count, _entityName, stopper.Elapsed.TotalMilliseconds, UserContextTask);
      }
    }

    public virtual async Task<bool> Delete(Guid appKey)
    {
      var stopper = new Stopwatch();
      stopper.Start();

      try
      {
	      if (UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
        var entity = await _dbSet.AsNoTracking().SingleOrDefaultAsync(e => e.AppKey == appKey);
        if (entity != null)
        {
          _dbSet.Remove(entity);
          await _context.SaveChangesAsync(UserContextTask);
          return _dbSet.AsNoTracking().SingleOrDefault(e => e.AppKey == appKey) == null;
        }

        return false;
      }
      catch (Exception e)
      {
	      _logger.Error(e, "Error when execute Delete [{@_entityName}] : {@appKey} | Context user: {@userContext}", _entityName, appKey, UserContextTask);
	      return false;
      }
      finally
      {
	      stopper.Stop();
	      _logger.Debug("Delete [{@_entityName}] to DB total time: {1} [ms] | Context user: {@userContext}", _entityName, stopper.Elapsed.TotalMilliseconds, UserContextTask);
      }
    }

    public virtual async Task<IDictionary<Guid, bool>> DeleteRange(ICollection<Guid> appKeys)
    {
      var stopper = new Stopwatch();
      stopper.Start();
      var returnDictionary = new Dictionary<Guid, bool>();
      try
      {
	      if (UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
        var entities = _dbSet.AsNoTracking().Where(x => appKeys.Contains(x.AppKey));
        if (appKeys.Count == entities.Count())
        {
          _dbSet.RemoveRange(entities);
          await _context.SaveChangesAsync(UserContextTask);

          foreach (var id in appKeys)
          {
            returnDictionary.Add(id, _dbSet.AsNoTracking().SingleOrDefault(x => x.AppKey == id) == null);
          }
        }

        return returnDictionary;
      }
      catch (Exception e)
      {
	      _logger.Error(e, "Error when execute DeleteRange [{@_entityName}] deleting list: {@returnDictionary} | Context user: {@userContext}", _entityName, returnDictionary, UserContextTask);
	      return returnDictionary;
      }
      finally
      {
	      stopper.Stop();
	      _logger.Debug("Delete {0} [{@_entityName}] on DB total time: {2} [ms] | Context user: {@userContext}", appKeys.Count, _entityName, stopper.Elapsed.TotalMilliseconds, UserContextTask);
      }
    }

    public virtual async Task<bool> Update(TEntity entity)
    {
      var stopper = new Stopwatch();
      stopper.Start();

      try
      {
	      if (UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
        _context.Entry(entity).State = EntityState.Modified;
        await _context.SaveChangesAsync(UserContextTask);
        _context.Entry(entity).State = EntityState.Detached;
        return true;
      }
      catch (Exception e)
      {
	      _logger.Error(e, "Error when execute Update [{@_entityName}] with data: {@entity} | Context user: {@userContext}", _entityName, entity, UserContextTask);
	      return false;
      }
      finally
      {
	      stopper.Stop();
	      _logger.Debug("Update [{@_entityName}] to DB total time: {1} [ms] | Context user: {@userContext}", _entityName, stopper.Elapsed.TotalMilliseconds, UserContextTask);
      }
    }

    public virtual async Task<IDictionary<long, bool>> UpdateRange(ICollection<TEntity> entities)
    {
      var stopper = new Stopwatch();
      stopper.Start();
      var returnDictionary = new Dictionary<long, bool>();
      try
      {
	      if (UserContextTask.Equals(Guid.Empty)) throw new Exception("Context user is Guid.Empty");
        foreach (var entity in entities)
        {
          _context.Entry(entity).State = EntityState.Modified;
        }

        await _context.SaveChangesAsync(UserContextTask);

        foreach (var entity in entities)
        {
          _context.Entry(entity).State = EntityState.Detached;
          returnDictionary.Add(entity.DbKey, true);
        }

        return returnDictionary;
      }
      catch (Exception e)
      {
	      _logger.Error(e, "Error when execute UpdateRange [{@_entityName}] deleting list: {@returnDictionary} | Context user: {@userContext}", _entityName, returnDictionary, UserContextTask);
	      return returnDictionary;
      }
      finally
      {
	      stopper.Stop();
	      _logger.Debug("UpdateRange {0} [{@_entityName}] on DB total time: {2} [ms] | Context user: {@userContext}", entities.Count, _entityName, stopper.Elapsed.TotalMilliseconds, UserContextTask);
      }
    }

    public virtual async Task<TResponseData> GetByAppKey<TResponseData>(Guid appKey) where TResponseData : class
    {
      return _mapper.Map<TEntity, TResponseData>(await GetByAppKey(appKey));
    }

    public virtual Task<TEntity> GetByAppKey(Guid appKey)
    {
	    var stopper = new Stopwatch();
	    stopper.Start();
	    try
	    {
		    return _dbSet.AsNoTracking().SingleOrDefaultAsync(s => s.AppKey == appKey);
	    }
	    catch (Exception e)
	    {
		    _logger.Error(e, "Error when execute GetByAppKey");
		    return default;
	    }
	    finally
	    {
		    stopper.Stop();
		    _logger.Debug("GetByAppKey total time: {0} | appKey: {1}", stopper.Elapsed.TotalMilliseconds, appKey);
	    }
    }

    public virtual Task<PaginatedListResponseData<TResponseData>> GetFiltered<TResponseData>(Expression<Func<TEntity, bool>> expression = null, QueryFilterRequest filterRequest = null) where TResponseData : class
    {
	    var stopper = new Stopwatch();
	    stopper.Start();

	    try
	    {
		    filterRequest ??= new QueryFilterRequest();
		    var source = expression == null ? _dbSet.AsNoTracking() : _dbSet.AsNoTracking().Where(expression);
		    return Task.FromResult(PaginatedListResponseData<TResponseData>.Create(source, filterRequest, _mapper));

	    }
	    catch (Exception e)
	    {
		    _logger.Error(e, "Error when execute GetFiltered | Expression: {0} ", expression);
		    return default;
	    }
	    finally
	    {
		    stopper.Stop();
		    _logger.Debug("GetFiltered total time: {0} | Expression: {1}", stopper.Elapsed.TotalMilliseconds, expression);
	    }
    }

    public virtual async Task<TResponseData> SingleOrDefault<TResponseData>(Expression<Func<TEntity, bool>> expression) where TResponseData : class
    {
	    return _mapper.Map<TEntity, TResponseData>(await SingleOrDefault(expression));
    }

    public virtual Task<TEntity> SingleOrDefault(Expression<Func<TEntity, bool>> expression)
    {
	    var stopper = new Stopwatch();
	    stopper.Start();

	    try
	    {
		    if (expression == null)
		    {
			    throw new InvalidExpressionException("No expression defined!");
		    }

		    return _dbSet.AsNoTracking().SingleOrDefaultAsync(expression);
	    }
	    catch (Exception e)
	    {
		    if (expression != null)
		    {
			    _logger.Error(e, "Error when execute SingleOrDefault | Expression: {0} ", expression);
		    }
		    else
		    {
			    _logger.Error(e, "Error when execute SingleOrDefault | Expression: IS NULL!");
		    }
	    }
	    finally
	    {
		    stopper.Stop();
		    if (expression != null)
		    {
			    _logger.Debug("SingleOrDefault total time: {0} | Expression: {1}", stopper.Elapsed.TotalMilliseconds, expression);
		    }
		    else
		    {
			    _logger.Debug("SingleOrDefault total time: {0} | Expression: IS NULL!", stopper.Elapsed.TotalMilliseconds);
		    }
	    }

	    return default;
    }

    public virtual async Task<TResponseData> FirstOrDefault<TResponseData>(Expression<Func<TEntity, bool>> expression) where TResponseData : class
    {
	    return _mapper.Map<TEntity, TResponseData>(await FirstOrDefault(expression));
    }

    public virtual Task<TEntity> FirstOrDefault(Expression<Func<TEntity, bool>> expression)
    {
	    var stopper = new Stopwatch();
	    stopper.Start();

	    try
	    {
		    if (expression == null)
		    {
			    throw new InvalidExpressionException("No expression defined!");
		    }

		    return _dbSet.AsNoTracking().FirstOrDefaultAsync(expression);
	    }
	    catch (Exception e)
	    {
		    if (expression != null)
		    {
			    _logger.Error(e, "Error when execute FirstOrDefault | Expression: {0} ", expression);
		    }
		    else
		    {
			    _logger.Error(e, "Error when execute FirstOrDefault | Expression: IS NULL!");
		    }
	    }
	    finally
	    {
		    stopper.Stop();
		    if (expression != null)
		    {
			    _logger.Debug("FirstOrDefault total time: {0} | Expression: {1}", stopper.Elapsed.TotalMilliseconds, expression);
		    }
		    else
		    {
			    _logger.Debug("FirstOrDefault total time: {0} | Expression: IS NULL!", stopper.Elapsed.TotalMilliseconds);
		    }
	    }

	    return default;
    }
  }
}