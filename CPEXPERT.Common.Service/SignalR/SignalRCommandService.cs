﻿using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Helpers;
using CPEXPERT.Common.Service.SignalR.ContractsData;
using Serilog;

namespace CPEXPERT.Common.Service.SignalR
{
	public class SignalRCommandService : ISignalRCommandService
	{
		public IHttpServiceProvider HttpServiceProvider { get; }
		private readonly ILogger _logger;

		public SignalRCommandService(IHttpServiceProvider httpServiceProvider, ILogger logger)
		{
			_logger = logger;
			HttpServiceProvider = httpServiceProvider;
			HttpServiceProvider.CreateHttpClient("SignalR", "Command");
		}

		public Task ExecuteSuccess(Request<CommandMessagesToUserData> request)
		{
			_logger.Debug("[CPEXPERT.Core.Service.SignalR.SignalRCommandService].ExecuteSuccess(...) -> Execute");
			return HttpServiceProvider.Post<object, CommandMessagesToUserData>("ExecuteSuccess", request, AuthHelper.GetSystemToken());
		}

		public Task ExecuteValidationFailure(Request<CommandMessagesToUserData> request)
		{
			_logger.Debug("[CPEXPERT.Core.Service.SignalR.SignalRCommandService].ExecuteValidationFailure(...) -> Execute");
			return HttpServiceProvider.Post<object, CommandMessagesToUserData>("ExecuteValidationFailure", request, AuthHelper.GetSystemToken());
		}
	}
}