﻿using System;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Helpers;
using Serilog;

namespace CPEXPERT.Common.Service.SignalR
{
	public class SignalRSystemRemoteMethodsService : ISignalRSystemRemoteMethodsService
	{
		public IHttpServiceProvider HttpServiceProvider { get; }
		private readonly ILogger _logger;

		public SignalRSystemRemoteMethodsService(IHttpServiceProvider httpServiceProvider, ILogger logger)
		{
			_logger = logger;
			HttpServiceProvider = httpServiceProvider;
			HttpServiceProvider.CreateHttpClient("SignalR", "SystemRemoteMethods");
		}

		public Task RefreshUserProfile(Request<Guid> request)
		{
			_logger.Debug("[CPEXPERT.Core.Service.SignalR.SignalRSystemRemoteMethodsService].RefreshUserProfile(...) -> Execute");
			return HttpServiceProvider.Post<object, Guid>("RefreshUserProfile", request, AuthHelper.GetSystemToken());
		}
	}
}