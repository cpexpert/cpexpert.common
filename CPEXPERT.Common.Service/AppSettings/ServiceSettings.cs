﻿namespace CPEXPERT.Common.Service.AppSettings
{
	public class ServiceSettings
	{
		public string Name { get; set; }
		public string Url { get; set; }
		public bool? MockApiData { get; set; }
	}
}