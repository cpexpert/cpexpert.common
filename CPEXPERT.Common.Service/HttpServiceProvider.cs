﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Service.AppSettings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace CPEXPERT.Common.Service
{
	public class HttpServiceProvider : IHttpServiceProvider
	{
		public HttpServiceProvider(ILogger logger)
		{
			Logger = logger;
			MockApiData = false;
		}

		private ServiceSettings _serviceSettings { get; set; }
		public readonly ILogger Logger;
		private HttpClient _client { get; set; }
		public bool MockApiData { get; private set; }
		public string ControllerName { get; private set; }

		public void CreateHttpClient(string serviceName, string controllerName)
		{
			_client = new HttpClient();
			ControllerName = controllerName;
			var services = Helpers.ConfigurationHelper.GetConfigSection<ServiceSettings[]>("Services");
			_serviceSettings = services.SingleOrDefault(s => s.Name.Equals(serviceName));
			if (_serviceSettings == null)
			{
				throw new Exception($"Can't find {serviceName} service configuration!");
			}

			MockApiData = _serviceSettings.MockApiData ?? false;
		}

		public async Task<Response<TResponseData>> Get<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token)
		{
			if (_serviceSettings == null || string.IsNullOrEmpty(ControllerName) || _client == null)
			{
				throw new Exception("First use CreateHttpClient method!");
			}

			return await ExecuteHttpRequest<TResponseData, TRequestData>(HttpMethod.Get, CreateUrl(new [] { _serviceSettings.Url, ControllerName, methodName }), request, token);
    }
		public async Task<Response<TResponseData>> Get<TResponseData>(string methodName, string paramsFromQueryKey, string paramsFromQueryValue, string token)
		{
			if (_serviceSettings == null || string.IsNullOrEmpty(ControllerName) || _client == null)
			{
				throw new Exception("First use CreateHttpClient method!");
			}

			return await ExecuteHttpRequest<TResponseData>(HttpMethod.Get, $"{CreateUrl(new[] { _serviceSettings.Url, ControllerName, methodName })}?{paramsFromQueryKey}={paramsFromQueryValue}", token);
    }
		public async Task<Response<TResponseData>> Get<TResponseData>(string methodName, string token)
		{
			if (_serviceSettings == null || string.IsNullOrEmpty(ControllerName) || _client == null)
			{
				throw new Exception("First use CreateHttpClient method!");
			}

			return await ExecuteHttpRequest<TResponseData>(HttpMethod.Get, CreateUrl(new[] { _serviceSettings.Url, ControllerName, methodName }), token);
		}
		public async Task<Response<TResponseData>> Post<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token)
		{
			if (_serviceSettings == null || string.IsNullOrEmpty(ControllerName) || _client == null)
			{
				throw new Exception("First use CreateHttpClient method!");
			}

			return await ExecuteHttpRequest<TResponseData, TRequestData>(HttpMethod.Post, CreateUrl(new[] { _serviceSettings.Url, ControllerName, methodName }), request, token);
    }
		public async Task<Response<TResponseData>> Put<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token)
		{
			if (_serviceSettings == null || string.IsNullOrEmpty(ControllerName) || _client == null)
			{
				throw new Exception("First use CreateHttpClient method!");
			}

			return await ExecuteHttpRequest<TResponseData, TRequestData>(HttpMethod.Put, CreateUrl(new[] { _serviceSettings.Url, ControllerName, methodName }), request, token);
    }
		public async Task<Response<TResponseData>> Delete<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token)
		{
			if (_serviceSettings == null || string.IsNullOrEmpty(ControllerName) || _client == null)
			{
				throw new Exception("First use CreateHttpClient method!");
			}

			return await ExecuteHttpRequest<TResponseData, TRequestData>(HttpMethod.Delete, CreateUrl(new[] { _serviceSettings.Url, ControllerName, methodName }), request, token);
    }

		protected async Task<Response<TResponseData>> ExecuteHttpRequest<TResponseData, TRequestData>(HttpMethod method, string url, Request<TRequestData> request, string token)
    {
	    var result = new Response<TResponseData>();

	    try
      {
	      if (method == null || string.IsNullOrEmpty(url) || request == null)
	      {
					throw new Exception("[ServiceProvider.ExecuteHttpRequest] No all input params set");
	      }

	      Logger.Debug("[ServiceProvider.ExecuteHttpRequest] request: {@request}", request);
	      var content = new ObjectContent<Request<TRequestData>>(request, new JsonMediaTypeFormatter());

	      result = await CreateRequest(method, url, content, result, request, token);
      }
      catch (Exception e)
      {
				result.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				Logger.Error(e, "[ServiceProvider.ExecuteHttpRequest] Unhandled exception when execute http request on url: {@url}, method: {@method} and request: {@request}", url, method, request);
			}

      return result;
    }
		protected async Task<Response<TResponseData>> ExecuteHttpRequest<TResponseData>(HttpMethod method, string url, string token)
		{
			var result = new Response<TResponseData>();

			try
			{
				if (method == null || string.IsNullOrEmpty(url))
				{
					throw new Exception("[ServiceProvider.ExecuteHttpRequest] No all input params set");
				}

				Logger.Debug("[ServiceProvider.ExecuteHttpRequest] url: {@url}", url);
				result = await CreateRequest(method, url, null, result, null, token);
			}
			catch (Exception e)
			{
				result.Messages.Add(new MessageData { Language = Languages.English, Message = e.Message, Type = MessageTypes.Error });
				Logger.Error(e, "[ServiceProvider.ExecuteHttpRequest] Unhandled exception when execute http request on url: {@url}, method: {@method} and request: {@request}", url, method, null);
			}

			return result;
		}
		protected async Task<Response<TResponseData>> CreateRequest<TResponseData>(HttpMethod method, string url, HttpContent content, Response<TResponseData> result, object request, string token)
		{
			var requestMessage = new HttpRequestMessage(method, url)
			{
				Content = content,
				Headers = {Authorization = !string.IsNullOrEmpty(token) ? new AuthenticationHeaderValue("Bearer", token) : null}
			};

			var response = await _client.SendAsync(requestMessage).ConfigureAwait(false);
			await response.Content.ReadAsStringAsync().ContinueWith(x =>
			{
				if (x.IsFaulted)
				{
					result.Messages.Add(new MessageData { Language = Languages.English, Message = x.Exception.Message, Type = MessageTypes.Error });
					if (request == null)
					{
						Logger.Error(x.Exception, "[ServiceProvider.ExecuteHttpRequest] Error when execute http request on url: {@url}, method: {@method}", url, method);
					}
					else
					{
						Logger.Error(x.Exception, "[ServiceProvider.ExecuteHttpRequest] Error when execute http request on url: {@url}, method: {@method} and request: {@request}", url, method, request);
					}
				}
				else if (response.IsSuccessStatusCode)
				{
					var json = x.Result;
					Logger.Debug("[ServiceProvider.ExecuteHttpRequest] response JSON: " + (json ?? ""));

					try
					{
						result = JsonConvert.DeserializeObject<Response<TResponseData>>(json);
					}
					catch
					{
						result = ParseStringToTypedValue<Response<TResponseData>>(json);
					}
				}
				else
				{
					result.Messages.Add(new MessageData
					{
						Language = Languages.English,
						Message = "Unhandled exception when execute http request",
						Type = MessageTypes.Error
					});
					if (request == null)
					{
						Logger.Error("[ServiceProvider.ExecuteHttpRequest] Unhandled exception when execute http request on url: {@url}, method: {@method}", url, method);
					}
					else
					{
						Logger.Error("[ServiceProvider.ExecuteHttpRequest] Unhandled exception when execute http request on url: {@url}, method: {@method} and request: {@request}", url, method, request);
					}
				}
			});

			return result;
		}
		protected static T ParseStringToTypedValue<T>(string value)
		{
			return typeof(T) == typeof(string) ? JToken.Parse($"\"{value}\"").Value<T>() : JToken.Parse(value).Value<T>();
		}
		protected static string CreateUrl(string[] urlElements)
		{
			return string.Join('/', urlElements);
		}
	}
}