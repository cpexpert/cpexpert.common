﻿using Autofac;
using CPEXPERT.Common.Service.SignalR;
using Serilog;

namespace CPEXPERT.Common.Service.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<HttpServiceProvider>().As<IHttpServiceProvider>().InstancePerLifetimeScope();
			builder.RegisterType<SignalRCommandService>().As<ISignalRCommandService>().InstancePerLifetimeScope();
			builder.RegisterType<SignalRSystemRemoteMethodsService>().As<ISignalRSystemRemoteMethodsService>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.Common.Service.Autofac].Load(...)");
		}
	}
}
