﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Domain.Enums;

namespace CPEXPERT.Common.DAL.Mongo
{
  public abstract class DataBaseContext : IDataContext
  {
	  public bool Migrate()
	  {
		  throw new NotImplementedException();
	  }

	  public int SaveChanges(Guid contextUserId)
	  {
		  throw new NotImplementedException();
	  }

	  public Task<int> SaveChangesAsync(Guid contextUserId, CancellationToken cancellationToken)
	  {
		  throw new NotImplementedException();
	  }

	  public DataBaseTypes DbType => DataBaseTypes.Mongo;
	  public bool? ForcedExecuteSeed { get; private set; }
	  public abstract bool Seed();
	}
}