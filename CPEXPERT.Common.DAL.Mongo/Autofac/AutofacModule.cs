﻿using Autofac;
using Serilog;

namespace CPEXPERT.Common.DAL.Mongo.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			Log.Debug("[CPEXPERT.Common.DAL.Mongo.Autofac].Load(...)");
		}
	}
}
