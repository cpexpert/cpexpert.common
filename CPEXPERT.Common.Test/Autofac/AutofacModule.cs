﻿using Autofac;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Event;
using CPEXPERT.Common.Service.SignalR;
using CPEXPERT.Common.Test.DbStore;
using Serilog;

namespace CPEXPERT.Common.Test.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<TestDataStore>().AsSelf().InstancePerLifetimeScope();
			builder.RegisterType<FakeRepository>().As<IBaseRepositoryAsync<FakeEntity, long>>().InstancePerLifetimeScope();
			builder.RegisterType<TestLogger>().As<ILogger>().InstancePerLifetimeScope();
			builder.RegisterType<TestSignalRCommandService>().As<ISignalRCommandService>().InstancePerLifetimeScope();
			builder.RegisterType<TestEventBus>().As<IEventBus>().InstancePerLifetimeScope();
			builder.RegisterType<TestUserDataProvider>().As<IUserDataProvider>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.Common.Test.Autofac].Load(...)");
		}
	}
}