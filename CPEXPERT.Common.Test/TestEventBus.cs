﻿using System.Threading.Tasks;
using CPEXPERT.Common.Event;

namespace CPEXPERT.Common.Test
{
	public class TestEventBus : IEventBus
	{
		public Task Publish<TEventData>(EventMessage<TEventData> eventMessage)
		{
			return Task.CompletedTask;
		}
	}
}