﻿using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Service;
using CPEXPERT.Common.Service.SignalR;
using CPEXPERT.Common.Service.SignalR.ContractsData;

namespace CPEXPERT.Common.Test
{
	public class TestSignalRCommandService : ISignalRCommandService
	{
		public IHttpServiceProvider HttpServiceProvider { get; }
		public Task ExecuteSuccess(Request<CommandMessagesToUserData> request)
		{
			return Task.CompletedTask;
		}

		public Task ExecuteValidationFailure(Request<CommandMessagesToUserData> request)
		{
			return Task.CompletedTask;
		}
	}
}