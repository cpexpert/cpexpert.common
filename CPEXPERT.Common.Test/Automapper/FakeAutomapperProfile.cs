﻿using AutoMapper;
using CPEXPERT.Common.Test.DbStore;
using CPEXPERT.Common.Test.ResponseData;

namespace CPEXPERT.Common.Test.Automapper
{
	public class FakeAutomapperProfile : Profile
	{
		public FakeAutomapperProfile()
		{
			CreateMap<FakeEntity, FakeResponseData>();
			CreateMap<FakeResponseData, FakeEntity>();
		}
	}
}