﻿using System;
using CPEXPERT.Common.Abstracts;

namespace CPEXPERT.Common.Test
{
	public class TestUserDataProvider : IUserDataProvider
	{
		public Guid GetCurrentUserAppKey()
		{
			return Guid.NewGuid();
		}
	}
}