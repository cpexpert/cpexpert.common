﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using CPEXPERT.Common.ContractsData;
using CPEXPERT.Common.DAL.Interfaces;
using CPEXPERT.Common.Filters;

namespace CPEXPERT.Common.Test.DbStore
{
	public class FakeRepository : IBaseRepositoryAsync<FakeEntity, long>
	{
		protected readonly TestDataStore _testDataStore;
		protected readonly IMapper _mapper;

		public FakeRepository(IMapper mapper, TestDataStore testDataStore)
		{
			_mapper = mapper;
			_testDataStore = testDataStore;
		}

		public void Dispose()
		{
		}

		public Task<long> Add(FakeEntity entity)
		{
			entity.DbKey = _testDataStore.Entities.Select(s => s.DbKey).Max() + 1;
			_testDataStore.Entities.Add(entity);
			return Task.FromResult(entity.DbKey);
		}

		public Task<ICollection<long>> AddRange(ICollection<FakeEntity> entities)
		{
			var listOfIds = new List<long>();
			foreach (var item in entities)
			{
				listOfIds.Add(Add(item).Result);
			}

			return Task.FromResult<ICollection<long>>(listOfIds);
		}

		public Task<bool> Delete(Guid appKey)
		{
			_testDataStore.Entities.Remove(_testDataStore.Entities.SingleOrDefault(s => s.AppKey == appKey));
			return Task.FromResult(true);
		}

		public Task<IDictionary<long, bool>> DeleteRange(ICollection<long> ids)
		{
			var listOfIds = new Dictionary<long, bool>();
			foreach (var id in ids)
			{
				var itemAppKey = _testDataStore.Entities.SingleOrDefault(s => s.DbKey == id).AppKey;
				listOfIds.Add(id, Delete(itemAppKey).Result);
			}

			return Task.FromResult<IDictionary<long, bool>>(listOfIds);
		}

		public Task<IDictionary<Guid, bool>> DeleteRange(ICollection<Guid> appKeys)
		{
			var listOfIds = new Dictionary<Guid, bool>();
			foreach (var id in appKeys)
			{
				listOfIds.Add(id, Delete(id).Result);
			}

			return Task.FromResult<IDictionary<Guid, bool>>(listOfIds);
		}

		public Task<bool> Update(FakeEntity entity)
		{
			var currentData = _testDataStore.Entities.SingleOrDefault(s => s.AppKey == entity.AppKey);
			if (currentData != null)
			{
				_testDataStore.Entities.Remove(currentData);
				_testDataStore.Entities.Add(entity);
			}

			return Task.FromResult(false);
		}

		public Task<IDictionary<long, bool>> UpdateRange(ICollection<FakeEntity> entities)
		{
			var listOfIds = new Dictionary<long, bool>();
			foreach (var item in entities)
			{
				listOfIds.Add(item.DbKey, Update(item).Result);
			}

			return Task.FromResult<IDictionary<long, bool>>(listOfIds);
		}

		public Task<TResponseData> GetByAppKey<TResponseData>(Guid appKey) where TResponseData : class
		{
			return Task.FromResult(_mapper.Map<FakeEntity, TResponseData>(_testDataStore.Entities.FirstOrDefault(x => x.AppKey == appKey)));
		}

		public Task<FakeEntity> GetByAppKey(Guid appKey)
		{
			return Task.FromResult(_testDataStore.Entities.FirstOrDefault(x => x.AppKey == appKey));
		}

		public Task<PaginatedListResponseData<TResponseData>> GetFiltered<TResponseData>(
			Expression<Func<FakeEntity, bool>> expression = null, QueryFilterRequest filterRequest = null)
			where TResponseData : class
		{
			filterRequest ??= new QueryFilterRequest();
			return Task.FromResult(PaginatedListResponseData<TResponseData>.Create(
				_testDataStore.Entities.AsQueryable().Where(expression), filterRequest, _mapper));
		}

		public Task<TResponseData> SingleOrDefault<TResponseData>(Expression<Func<FakeEntity, bool>> expression) where TResponseData : class
		{
			return Task.FromResult(_mapper.Map<FakeEntity, TResponseData>(_testDataStore.Entities.AsQueryable().SingleOrDefault(expression)));
		}

		public Task<FakeEntity> SingleOrDefault(Expression<Func<FakeEntity, bool>> expression)
		{
			return Task.FromResult(_testDataStore.Entities.AsQueryable().SingleOrDefault(expression));
		}

		public Task<TResponseData> FirstOrDefault<TResponseData>(Expression<Func<FakeEntity, bool>> expression) where TResponseData : class
		{
			return Task.FromResult(_mapper.Map<FakeEntity, TResponseData>(_testDataStore.Entities.AsQueryable().FirstOrDefault(expression)));
		}

		public Task<FakeEntity> FirstOrDefault(Expression<Func<FakeEntity, bool>> expression)
		{
			return Task.FromResult(_testDataStore.Entities.AsQueryable().FirstOrDefault(expression));
		}
	}
}