﻿using System;
using CPEXPERT.Common.Domain.Entities;

namespace CPEXPERT.Common.Test.DbStore
{
    public class FakeEntity : BaseEntity<long>
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public DateTime Date { get; set; }
    }
}