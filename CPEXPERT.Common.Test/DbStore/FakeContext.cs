﻿using System.Collections.Generic;

namespace CPEXPERT.Common.Test.DbStore
{
    public class TestDataStore
    {
        public TestDataStore()
        {
            Entities = GenFu.GenFu.ListOf<FakeEntity>(1000);
        }

        public ICollection<FakeEntity> Entities { get; set; }
    }
}