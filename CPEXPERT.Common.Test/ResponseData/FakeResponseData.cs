﻿using System;

namespace CPEXPERT.Common.Test.ResponseData
{
	public class FakeResponseData
	{
		public long DbKey { get; set; }
		public Guid AppKey { get; set; }
		public DateTime CreateDateUtc { get; set; }
		public Guid CreateUser { get; set; }
		public DateTime LastEditDateUtc { get; set; }
		public Guid LastEditUser { get; set; }
		public string Name { get; set; }
		public int Number { get; set; }
		public DateTime Date { get; set; }
	}
}