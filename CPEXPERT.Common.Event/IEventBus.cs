﻿using System.Threading.Tasks;

namespace CPEXPERT.Common.Event
{
	public interface IEventBus
	{
		Task Publish<TEventData>(EventMessage<TEventData> eventMessage);
	}
}