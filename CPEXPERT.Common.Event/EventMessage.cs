﻿using System;

namespace CPEXPERT.Common.Event
{
	public class EventMessage<TEventData>
	{
		public EventMessage()
		{
			Id = Guid.NewGuid();
			Created = DateTime.UtcNow;
			Data = default;
			ContextUserAppKey = Guid.Empty;
		}

		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public TEventData Data { get; set; }
		public Guid ContextUserAppKey { get; set; }
	}
}