﻿namespace CPEXPERT.Common.Event.AppSettings
{
	public class RebusSettings
	{
		public string ConnectionString { get; set; }
		public string InputQueue { get; set; }
		public string ExchangeName { get; set; }
		public int NumberOfWorkers { get; set; }
		public int MaxParallelism { get; set; }
	}
}