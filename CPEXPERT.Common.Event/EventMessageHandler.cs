﻿using System.Threading.Tasks;
using Rebus.Handlers;
using Serilog;

namespace CPEXPERT.Common.Event
{
	public abstract class EventMessageHandler<TEventMessage, TEventData> : IHandleMessages<TEventMessage> where TEventMessage : EventMessage<TEventData>
	{
		public readonly ILogger Logger;

		protected EventMessageHandler(ILogger logger)
		{
			Logger = logger;
		}
		public abstract Task Handle(TEventMessage eventMessage);
	}
}