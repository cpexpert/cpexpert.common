﻿using Autofac;
using Serilog;

namespace CPEXPERT.Common.Event.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<EventMessageBus>().As<IEventBus>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.Common.Event.Autofac].Load(...)");
		}
	}
}
