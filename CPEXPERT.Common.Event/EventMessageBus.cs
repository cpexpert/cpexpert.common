﻿using System;
using System.Threading.Tasks;
using Rebus.Bus;
using Serilog;

namespace CPEXPERT.Common.Event
{
	public class EventMessageBus : IEventBus
	{
		private readonly IBus _bus;
		private readonly ILogger _logger;

		public EventMessageBus(IBus bus, ILogger logger)
		{
			_bus = bus;
			_logger = logger;
		}

		public Task Publish<TEventData>(EventMessage<TEventData> eventMessage)
		{
			_logger.Debug("Start publish event: {@param}", eventMessage);
			try
			{
				_bus.Publish(eventMessage);
				_logger.Debug("Success publish event: {@param}", eventMessage);
			}
			catch (Exception exception)
			{
				_logger.Error(exception, "Error when publish event: {@param}", eventMessage);
			}
			finally
			{
				_logger.Debug("Finish publish event: {@param}", eventMessage);
			}

			return Task.CompletedTask;
		}
	}
}