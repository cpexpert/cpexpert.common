﻿using System;
using CPEXPERT.Common.Contracts;
using FluentValidation.Results;
using MediatR;

namespace CPEXPERT.Common.Command
{
	public class Command<TCommandData> : INotification, IRequest<Response<ValidationResult>>
	{
		public Command()
		{
			Id = Guid.NewGuid();
			Created = DateTime.UtcNow;
			RequestId = Guid.Empty;
			RequestCreated = DateTime.MinValue;
			Finished = null;
			CommandData = default;
			InsertedOnQueue = null;
		}

		public Guid Id { get; }
	  public DateTime Created { get; }
	  public Guid RequestId { get; set; }
	  public DateTime RequestCreated { get; set; }
	  public DateTime? InsertedOnQueue { get; set; }
		public DateTime? Finished { get; set; }
	  public TCommandData CommandData { get; set; }
	}
}