﻿using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using FluentValidation.Results;
using MediatR;

namespace CPEXPERT.Common.Command
{
	public class CommandBus : ICommandBus
	{
		private readonly IMediator _mediator;
		public CommandBus(IMediator mediator)
		{
			_mediator = mediator;
		}

		public Task<Response<ValidationResult>> ValidateAndPublish<TCommandData>(Command<TCommandData> command)
		{
			return _mediator.Send(command);
		}

		public Task Execute<TCommandData>(Command<TCommandData> command)
		{
			return _mediator.Publish(command);
		}
	}
}