﻿using Autofac;
using CPEXPERT.Common.Command.Validators;
using FluentValidation;
using Serilog;

namespace CPEXPERT.Common.Command.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<FluentValidatorFactory>().As<IValidatorFactory>().InstancePerDependency();
			builder.RegisterType<CommandBus>().As<ICommandBus>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.Common.Command.Autofac].Load(...)");
		}
	}
}
