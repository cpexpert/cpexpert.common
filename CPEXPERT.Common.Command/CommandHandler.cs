﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPEXPERT.Common.Abstracts;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Event;
using CPEXPERT.Common.Service.SignalR;
using CPEXPERT.Common.Service.SignalR.ContractsData;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Serilog;

namespace CPEXPERT.Common.Command
{
	public abstract class CommandHandler<TCommand, TCommandData> : INotificationHandler<TCommand>, IRequestHandler<TCommand, Response<ValidationResult>> 
		where TCommand : Command<TCommandData>
	{
		private readonly IValidatorFactory _validatorFactory;
		private readonly ISignalRCommandService _signalRCommandService;
		private readonly IEventBus _eventBus;

		public readonly ILogger Logger;
		public readonly Guid UserContextTask;

		protected CommandHandler(IValidatorFactory validatorFactory, ILogger logger, ISignalRCommandService signalRCommandService, IEventBus eventBus, IUserDataProvider userContextTask)
		{
			_validatorFactory = validatorFactory;
			Logger = logger;
			_signalRCommandService = signalRCommandService;
			_eventBus = eventBus;
			UserContextTask = userContextTask.GetCurrentUserAppKey();
		}

		// RE-VALIDATE AND EXECUTE
		public virtual async Task Handle(TCommand command, CancellationToken cancellationToken)
		{
			Logger.Debug("Start RE-VALIDATE AND EXECUTE command: {@command}", command);

			try
			{
				var validationResult = await Validate(command.CommandData, cancellationToken);
				if (validationResult.IsValid)
				{
					var returnMessageAfterExecuteCommand = await Execute(command, cancellationToken);
					if (returnMessageAfterExecuteCommand != null)
					{
						command.Finished = DateTime.UtcNow;
						await StoreCommandOnEventStore(command);
						await ExecuteSuccessAction(UserContextTask, returnMessageAfterExecuteCommand);
					}
					else
					{
						throw new Exception("Unhandled exception on execute command");
					}
				}
				else if (validationResult.Errors != null && validationResult.Errors.Any())
				{
					await ValidationFailureAction(validationResult, UserContextTask);
				}
				else
				{
					throw new Exception("Unhandled exception on validate command");
				}
			}
			catch (Exception exception)
			{
				Logger.Error(exception, "Internal error when RE-VALIDATE AND EXECUTE command: {@command}", command);
				await OnExceptionAction(exception, UserContextTask);
				throw;
			}
			finally
			{
				Logger.Debug("Finish RE-VALIDATE AND EXECUTE command: {@command}", command);
			}
		}

		// VALIDATE AND INSERT COMMAND TO EXECUTE
		async Task<Response<ValidationResult>> IRequestHandler<TCommand, Response<ValidationResult>>.Handle(TCommand command, CancellationToken cancellationToken)
		{
			Logger.Debug("Start VALIDATE AND INSERT ON QUEUE command: {@command}", command);

			try
			{
				var validationResult = await Validate(command.CommandData, cancellationToken);
				if (validationResult.IsValid)
				{
					command.InsertedOnQueue = DateTime.UtcNow;
					// insert command on queue
					await _eventBus.Publish(new EventMessage<TCommand> { Data = command });
				}

				return new Response<ValidationResult>
				{
					CanCache = false,
					RequestCreated = command.RequestCreated,
					RequestId = command.RequestId,
					ResponseData = validationResult,
					Messages = validationResult.Errors.Select(s => new MessageData
					{
						Language = Languages.English,
						Message = s.ErrorMessage,
						Type = MessageTypes.Error
					}).ToList()
				};
			}
			catch (Exception exception)
			{
				Logger.Error(exception, "Internal error when VALIDATE AND INSERT ON QUEUE command: {@command}", command);
				await OnExceptionAction(exception, UserContextTask);
				throw;
			}
			finally
			{
				Logger.Debug("Finish VALIDATE AND INSERT ON QUEUE command: {@command}", command);
			}
		}

		public virtual async Task OnExceptionAction(Exception exception, Guid executedUserAppKey)
		{
			await _signalRCommandService.ExecuteValidationFailure(new Request<CommandMessagesToUserData>
			{
				CanCache = false,
				Language = Languages.English,
				RequestData = new CommandMessagesToUserData
				{
					UserAppKey = executedUserAppKey,
					Messages = new List<MessageData> { new MessageData { Type = MessageTypes.Error, Language = Languages.English, Message = exception.Message } }.ToArray()
				}
			});
		}

		public virtual async Task ValidationFailureAction(ValidationResult validationResult, Guid executedUserAppKey)
		{
			await _signalRCommandService.ExecuteValidationFailure(new Request<CommandMessagesToUserData>
			{
				CanCache = false,
				Language = Languages.English,
				RequestData = new CommandMessagesToUserData
				{
					UserAppKey = executedUserAppKey,
					Messages = new List<MessageData>(validationResult.Errors.Select(s => new MessageData
					{
						Language = Languages.English,
						Message = s.ErrorMessage,
						Type = MessageTypes.Warning
					})).ToArray()
				}
			});
		}

		public virtual async Task ExecuteSuccessAction(Guid executedUserAppKey, MessageData message)
		{
			if (message != null)
			{
				if (message.Type == MessageTypes.Information)
				{
					await _signalRCommandService.ExecuteSuccess(new Request<CommandMessagesToUserData>
					{
						CanCache = false,
						Language = Languages.English,
						RequestData = new CommandMessagesToUserData
						{
							UserAppKey = executedUserAppKey,
							Messages = new List<MessageData> { message }.ToArray()
						}
					});
				}
				else
				{
					throw new Exception($"ExecuteSuccessAction try send message type: {message.Type}! Full message: {message}");
				}
			}
			else
			{
				throw new Exception($"ExecuteSuccessAction try send message type: {MessageTypes.Error}! Full message: IS NULL!");
			}
		}

		public abstract Task<MessageData> Execute(TCommand command, CancellationToken cancellationToken);
		public abstract Task StoreCommandOnEventStore(TCommand command);

		private Task<ValidationResult> Validate(TCommandData commandData, CancellationToken cancellationToken)
		{
			Logger.Debug("Start validate command: {@commandData}", commandData);

			try
			{
				var validator = _validatorFactory.GetValidator<TCommandData>();
				if (validator == null)
				{
					throw new Exception("Can't find validator");
				}

				return validator.ValidateAsync(new ValidationContext<TCommandData>(commandData), cancellationToken);
			}
			finally
			{
				Logger.Debug("Finish validate command: {@commandData}", commandData);
			}
		}
	}
}