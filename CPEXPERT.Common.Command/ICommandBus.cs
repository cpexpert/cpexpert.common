﻿using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using FluentValidation.Results;

namespace CPEXPERT.Common.Command
{
	public interface ICommandBus
	{
		Task<Response<ValidationResult>> ValidateAndPublish<TCommandData>(Command<TCommandData> command);
		Task Execute<TCommandData>(Command<TCommandData> command);
	}
}