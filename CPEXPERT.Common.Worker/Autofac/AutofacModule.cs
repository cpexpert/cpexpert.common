﻿
using Autofac;
using CPEXPERT.Common.Abstracts;
using Serilog;

namespace CPEXPERT.Common.Worker.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			builder.RegisterType<WorkerUserDataProvider>().As<IUserDataProvider>().InstancePerLifetimeScope();
			Log.Debug("[CPEXPERT.Common.WebApi.Autofac].Load(...)");
		}
	}
}
