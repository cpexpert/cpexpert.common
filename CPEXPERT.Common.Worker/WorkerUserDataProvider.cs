﻿using System;
using CPEXPERT.Common.Abstracts;

namespace CPEXPERT.Common.Worker
{
	public class WorkerUserDataProvider : IUserDataProvider
	{
		public virtual Guid GetCurrentUserAppKey()
		{
			return Guid.NewGuid();
		}
	}
}