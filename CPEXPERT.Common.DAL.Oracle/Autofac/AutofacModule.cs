﻿using Autofac;
using Serilog;

namespace CPEXPERT.Common.DAL.Oracle.Autofac
{
	public class AutofacModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			Log.Debug("[CPEXPERT.Common.DAL.Oracle.Autofac].Load(...)");
		}
	}
}
