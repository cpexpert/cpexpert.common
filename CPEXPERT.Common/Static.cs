﻿using System;
using System.Text.RegularExpressions;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common
{
	public static class Statics
	{
		public static class RegexExpressions
		{
			public static bool EmailIsMatch(string email)
			{
				return !string.IsNullOrEmpty(email) && Regex.IsMatch(email,
					       @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
					       @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
					       RegexOptions.IgnoreCase);
			}
		}

		public static class Users
		{
			public static class SuperAdmin
			{
				public static Guid Id = new Guid("86dcbe70-2ddb-4558-9e33-bc62740e2e15");
				public static string Email = "sa@cp-expert.pl";
				public static string DefaultPassword = "P@ssw0rd32167";
				public static SystemUserTypes UserType = SystemUserTypes.SuperAdministrator;
			}
			public static class TechSystemUser
			{
				public static Guid Id = new Guid("4c6e2b80-90b3-4577-8f10-02a67eff09fc");
				public static string Email = "tech@cp-expert.pl";
				public static string DefaultPassword = "t6zcs%2g^%SH^M+d";
				public static SystemUserTypes UserType = SystemUserTypes.Tech;
			}
			public static class DemoUser
			{
				public static Guid Id = new Guid("4b34b543-f826-49fd-acd3-8f1299d37334");
				public static string Email = "demo@cp-expert.pl";
				public static string DefaultPassword = "P@ssw0rd";
				public static SystemUserTypes UserType = SystemUserTypes.User;
			}
			public static class DemoAdminUser
			{
				public static Guid Id = new Guid("b609aa4d-ca0e-45fb-8fe9-31938de0a9e0");
				public static string Email = "demo-admin@cp-expert.pl";
				public static string DefaultPassword = "P@ssw0rd";
				public static SystemUserTypes UserType = SystemUserTypes.Administrator;
			}
			public static class AgentUser
			{
				public static Guid Id = new Guid("c115bb94-9347-43d1-b7f8-355a4f0924c4");
				public static string Email = "agent@cp-expert.pl";
				public static string DefaultPassword = "P@ssw0rd";
				public static SystemUserTypes UserType = SystemUserTypes.Agent;
			}
		}

		public static class Organizations
		{
			public static class Technical
			{
				public static Guid Id = new Guid("fb1b42d3-9011-4ead-bee7-069ca55774dc");
				public static string Name = "EMPTY_SA_TECH";
				public static string Description = "EMPTY_SA_TECH";
				public static SystemOrganizationTypes OrganizationType = SystemOrganizationTypes.Technical;
			}

			public static class Demo
			{
				public static Guid Id = new Guid("bd5a6827-a89c-4010-b274-af4521e336cd");
				public static string Name = "DEMO Organization";
				public static string Description = "DEMO Organization, only for demo user";
				public static SystemOrganizationTypes OrganizationType = SystemOrganizationTypes.Customer;
			}

			public static class Agency
			{
				public static Guid Id = new Guid("1fa7c869-f342-4d4d-b48e-8a140eebc4cd");
				public static string Name = "Demo Agency";
				public static string Description = "Demo agency organization";
				public static SystemOrganizationTypes OrganizationType = SystemOrganizationTypes.Agency;
			}
		}
	}
}