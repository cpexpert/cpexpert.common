﻿using System;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common.ContractsData.Auth
{
	public class JwtData
	{
		public Guid UserAppKey { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime ExpirationDate { get; set; }
		public Role[] Roles { get; set; }
		public SystemUserTypes UserType { get; set; }
		public Guid? OrganizationAppKey { get; set; }
	}
}