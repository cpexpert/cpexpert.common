﻿using System.Collections.Generic;

namespace CPEXPERT.Common.ContractsData.Auth
{
	public class Role
	{
		public string RoleName { get; set; }
		public ICollection<Permission> Permissions { get; set; }
	}
}