﻿using System.Collections.Generic;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common.ContractsData.Auth
{
	public class Permission
	{
		public string PermissionName { get; set; }
		public string ElementCode { get; set; }
		public List<SecurityAccessTypes> ElementSecurityAccessSchemaList { get; set; }
	}
}