﻿namespace CPEXPERT.Common.ContractsData.Auth
{
	public class AuthRequestData
	{
		public string Email { get; set; }
		public string Password { get; set; }
	}
}