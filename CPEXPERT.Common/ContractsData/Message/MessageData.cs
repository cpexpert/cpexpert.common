﻿using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common.ContractsData.Message
{
	public class MessageData
	{
		public MessageTypes Type { get; set; }
		public string Message { get; set; }
		public Languages Language { get; set; }
	}
}