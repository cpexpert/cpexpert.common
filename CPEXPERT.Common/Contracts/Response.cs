﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CPEXPERT.Common.ContractsData.Message;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common.Contracts
{
	public class Response<TResponseData>
	{
		public Response()
		{
			Id = Guid.NewGuid();
			Created = DateTime.UtcNow;
			RequestId = Guid.Empty;
			RequestCreated = DateTime.MinValue;
			Messages = new List<MessageData>();
			ResponseData = default;
			CanCache = false;
		}

		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public Guid RequestId { get; set; }
		public DateTime RequestCreated { get; set; }
		public ICollection<MessageData> Messages { get; set; }

		[NotMapped]
		public bool IsSuccess
		{
			get
			{
				return Messages == null || (Messages.All(a => a.Type != MessageTypes.Error) && Messages.All(a => a.Type != MessageTypes.Warning));
			}
		}

		public TResponseData ResponseData { get; set; }
		public bool CanCache { get; set; }
	}
}