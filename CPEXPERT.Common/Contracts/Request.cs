﻿using System;
using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common.Contracts
{
	public class Request<TRequestData>
	{
		public Request()
		{
			Id = Guid.NewGuid();
			Created = DateTime.UtcNow;
			CanCache = false;
			RequestData = default;
		}

		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public TRequestData RequestData { get; set; }
		public bool CanCache { get; set; }
		public Languages Language { get; set; }
	}
}