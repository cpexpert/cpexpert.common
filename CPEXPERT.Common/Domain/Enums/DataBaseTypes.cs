﻿namespace CPEXPERT.Common.Domain.Enums
{
	public enum DataBaseTypes
	{
		Undefined = 0,
		Mongo = 10,
		MsSql = 20,
		MySql = 30,
		Oracle = 40,
		SqlLite = 50,
  }
}