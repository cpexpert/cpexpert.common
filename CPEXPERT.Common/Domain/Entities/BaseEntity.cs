﻿using System;

namespace CPEXPERT.Common.Domain.Entities
{
	public class BaseEntity<TDbKey> : ICloneable
	{
		public BaseEntity()
		{
			var nowUtc = DateTime.UtcNow;
			DbKey = default;
			AppKey = Guid.Empty;
			CreateDateUtc = nowUtc;
			LastEditDateUtc = nowUtc;
			CreateUser = Guid.Empty;
			LastEditUser = Guid.Empty;
		}

		public TDbKey DbKey { get; set; }

		public Guid AppKey { get; set; }

		public DateTime CreateDateUtc { get; set; }

		public Guid CreateUser { get; set; }

		public DateTime LastEditDateUtc { get; set; }

		public Guid LastEditUser { get; set; }

		public object Clone()
		{
			return MemberwiseClone();
		}
  }
}