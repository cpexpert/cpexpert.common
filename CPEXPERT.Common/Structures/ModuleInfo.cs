﻿using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common.Structures
{
	public class ModuleInfo
	{
		public string ModuleName { get; set; }
		public AppModules ModuleType { get; set; }
		public string AssemblyVersion { get; set; }
		public string AssemblyName { get; set; }
		public string Copyright { get; set; }
		public bool IsRequiredSystemModule { get; set; }
		public bool IsLicensedModule { get; set; }
	}
}