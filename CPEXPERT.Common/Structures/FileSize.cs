﻿using CPEXPERT.Common.Enums;

namespace CPEXPERT.Common.Structures
{
	public class FileSize
	{
		public double Size { get; set; }
		public FileSizeTypes SizeType { get; set; }
	}
}