﻿using System;
using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;

namespace CPEXPERT.Common.Service.SignalR
{
	public interface ISignalRSystemRemoteMethodsService : IBaseService
	{
		Task RefreshUserProfile(Request<Guid> request);
	}
}