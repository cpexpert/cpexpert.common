﻿using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;
using CPEXPERT.Common.Service.SignalR.ContractsData;

namespace CPEXPERT.Common.Service.SignalR
{
	public interface ISignalRCommandService : IBaseService
	{
		Task ExecuteSuccess(Request<CommandMessagesToUserData> request);
		Task ExecuteValidationFailure(Request<CommandMessagesToUserData> request);
	}
}