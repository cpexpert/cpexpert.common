﻿using System;
using CPEXPERT.Common.ContractsData.Message;

namespace CPEXPERT.Common.Service.SignalR.ContractsData
{
	public class CommandMessagesToUserData
	{
		public MessageData[] Messages { get; set; }
		public Guid UserAppKey { get; set; }
	}
}