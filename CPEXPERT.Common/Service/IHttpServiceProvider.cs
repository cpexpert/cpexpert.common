﻿using System.Threading.Tasks;
using CPEXPERT.Common.Contracts;

namespace CPEXPERT.Common.Service
{
	public interface IHttpServiceProvider
	{
		bool MockApiData { get; }
		string ControllerName { get; }
		void CreateHttpClient(string serviceName, string controllerName);
		Task<Response<TResponseData>> Get<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token);
		Task<Response<TResponseData>> Get<TResponseData>(string methodName, string paramsFromQueryKey, string paramsFromQueryValue, string token);
		Task<Response<TResponseData>> Get<TResponseData>(string methodName, string token);
		Task<Response<TResponseData>> Post<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token);
		Task<Response<TResponseData>> Put<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token);
		Task<Response<TResponseData>> Delete<TResponseData, TRequestData>(string methodName, Request<TRequestData> request, string token);
	}
}