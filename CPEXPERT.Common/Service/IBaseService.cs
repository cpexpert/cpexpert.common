﻿namespace CPEXPERT.Common.Service
{
	public interface IBaseService
	{
		IHttpServiceProvider HttpServiceProvider { get; }
	}
}