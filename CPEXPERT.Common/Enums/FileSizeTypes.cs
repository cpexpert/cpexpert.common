﻿namespace CPEXPERT.Common.Enums
{
	public enum FileSizeTypes
	{
		Byte = 0,
		KiloByte = 1,
		MegaByte = 2,
		GigaByte = 3,
		TeraByte = 4
	}
}