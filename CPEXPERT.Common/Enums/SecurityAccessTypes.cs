﻿namespace CPEXPERT.Common.Enums
{
	public enum SecurityAccessTypes
	{
		Read = 100,
		Write = 200,
		Remove = 300,
	}
}