﻿namespace CPEXPERT.Common.Enums
{
	public enum SystemOrganizationTypes
	{
		Technical = 0,
		Customer = 1,
		Agency = 2
	}
}
