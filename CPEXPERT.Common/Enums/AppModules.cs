﻿using System.ComponentModel;

namespace CPEXPERT.Common.Enums
{
	public enum AppModules
	{
		[Description("")]
		Core = 0,

		[Description("")]
		SignalR = 10,

		[Description("")]
		Cemetery = 20,

	}
}