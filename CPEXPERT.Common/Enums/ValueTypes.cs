﻿namespace CPEXPERT.Common.Enums
{
	public enum ValueTypes
	{
		Undefined = 0,
		Integer = 10,
		Double = 20,
		String = 30,
		Boolean = 40,
		Xml = 50
  }
}