﻿namespace CPEXPERT.Common.Enums
{
	public enum PriorityLevels
	{
		Low = 0,
		Medium = 10,
		High = 20,
		Urgent = 30
	}
}