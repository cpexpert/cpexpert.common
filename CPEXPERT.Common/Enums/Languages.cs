﻿namespace CPEXPERT.Common.Enums
{
	public enum Languages
	{
		Undefined = -1,
		Polish = 100,
		English = 200
	}
}