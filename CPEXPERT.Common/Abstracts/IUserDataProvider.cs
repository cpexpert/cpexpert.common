﻿using System;

namespace CPEXPERT.Common.Abstracts
{
	public interface IUserDataProvider
	{
		Guid GetCurrentUserAppKey();
	}
}