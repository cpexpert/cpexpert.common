﻿using System;
using System.Globalization;
using System.Linq;
using CPEXPERT.Common.Enums;
using CPEXPERT.Common.Structures;

namespace CPEXPERT.Common.Helpers
{
	public static class Extensions
	{
		public static FileSize GetSizeInMemory(this byte[] byteArray)
		{
			double length = Convert.ToDouble(byteArray.Length);
			int order = 0;
			int maxValue = (int)Enum.GetValues(typeof(FileSizeTypes)).Cast<FileSizeTypes>().Max();
			while (length >= 1024D && order < maxValue)
			{
				order++;
				length /= 1024;
			}

			return new FileSize
			{
				Size = length,
				SizeType = (FileSizeTypes)order
			};
		}

		public static int GetSizeInMemoryInKB(this byte[] byteArray)
		{
			return byteArray.Length / 1024;
		}
	}
}