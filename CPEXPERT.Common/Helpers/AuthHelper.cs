﻿using System;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CPEXPERT.Common.ContractsData.Auth;
using CPEXPERT.Common.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace CPEXPERT.Common.Helpers
{
  public static class AuthHelper
  {
	  private static string GetTokenFromHeader(HttpRequest request)
    {
	    return StringValues.IsNullOrEmpty(request.Headers["Authorization"]) ? string.Empty : request.Headers["Authorization"].ToString().Replace("Bearer ", "");
    }

    public static string GetTokenFromHttpContext(HttpContext context)
    {
	    return context?.Request == null ? string.Empty : GetTokenFromHeader(context.Request);
    }

    public static string GetSystemToken()
    {
	    var securityKey = ConfigurationHelper.GetConfigSection<string>("JwtToken:SystemAuthSecretKey");
	    if (!string.IsNullOrEmpty(securityKey))
	    {
		    return CreateAuthResponseData(Statics.Users.TechSystemUser.Id, Statics.Users.TechSystemUser.Email, securityKey, new JwtData
		    {
			    UserType = SystemUserTypes.SuperAdministrator,
			    ExpirationDate = DateTime.UtcNow.AddSeconds(10),
			    CreateDate = DateTime.UtcNow,
			    Roles = new Role[0],
					OrganizationAppKey = null
		    }).BarerToken;
	    }

	    return string.Empty;
    }

    public static Guid GetCurrentUserAppKey(HttpContext context)
    {
			var token = GetTokenFromHttpContext(context);
			if (!string.IsNullOrEmpty(token))
			{
				var jwtData = DecodeBarerToken(token);
				if (jwtData != null)
				{
					return jwtData.UserAppKey;
				}
			}

			return Guid.Empty;
    }

    public static JwtData DecodeBarerToken(string token)
    {
	    try
	    {
		    var handler = new JwtSecurityTokenHandler();
		    if (handler.ReadToken(token) is JwtSecurityToken securityToken)
		    {
					var createDateString = securityToken.Claims.First(claim => claim.Type == "CreateDate").Value.ToString(CultureInfo.CurrentCulture);
					var expirationDateString = securityToken.Claims.First(claim => claim.Type == "ExpirationDate").Value.ToString(CultureInfo.CurrentCulture);
					var userAppKeyString = securityToken.Claims.First(claim => claim.Type == "UserAppKey").Value.ToString();
					var organizationAppKeyString = securityToken.Claims.First(claim => claim.Type == "OrganizationAppKey").Value.ToString();

					var userAppKey = new Guid(userAppKeyString);
					var createDate = Convert.ToDateTime(createDateString);
			    var expirationDate = Convert.ToDateTime(expirationDateString);
			    var baseRole = (SystemUserTypes)(Convert.ToInt32(securityToken.Claims.First(claim => claim.Type == "UserType").Value));
			    var organizationAppKey = !string.IsNullOrEmpty(organizationAppKeyString) ? new Guid(organizationAppKeyString) : (Guid?) null; 


					return new JwtData
					{
						UserAppKey = userAppKey,
						CreateDate = createDate,
						ExpirationDate = expirationDate,
						UserType = baseRole,
						Roles = Newtonsoft.Json.JsonConvert.DeserializeObject(securityToken.Claims.First(claim => claim.Type == "Roles").Value) as Role[],
						OrganizationAppKey = organizationAppKey
					};
				}
	    }
	    catch (Exception e)
	    {
		    Log.Error(e, "Error when decode barer token");
	    }

	    return null;
		}

    public static AuthResponseData CreateAuthResponseData(Guid userAppKey, string userLogin, string systemAuthSecretKey, JwtData payloadData)

		{
	    if (userAppKey != Guid.Empty && !string.IsNullOrEmpty(userLogin) 
					&& !string.IsNullOrEmpty(systemAuthSecretKey) 
	        && systemAuthSecretKey.Length < 20 
	        && payloadData == null || payloadData.UserType == SystemUserTypes.Undefined && userAppKey != payloadData.UserAppKey)
	    {
		    return null;
	    }

			// validate dates
			if (payloadData.CreateDate >= payloadData.ExpirationDate)
			{
				throw new InvalidDataException("CreateDate >= ExpirationDate");
			}

			if (DateTime.UtcNow >= payloadData.ExpirationDate)
			{
				throw new InvalidDataException("DateTime.UtcNow >= ExpirationDate");
			}

	    var tokenHandler = new JwtSecurityTokenHandler();
	    var tokenDescriptor = new SecurityTokenDescriptor
	    {
		    Subject = new ClaimsIdentity(new []
		    {
					new Claim("UserAppKey", payloadData.UserAppKey.ToString()), 
			    new Claim("CreateDate", payloadData.CreateDate.ToString(CultureInfo.CurrentCulture)),
			    new Claim("ExpirationDate", payloadData.ExpirationDate.ToString(CultureInfo.CurrentCulture)),
			    new Claim("UserType", ((int)payloadData.UserType).ToString()),
			    new Claim("Roles", Newtonsoft.Json.JsonConvert.SerializeObject(payloadData.Roles)),
			    new Claim("OrganizationAppKey", payloadData.OrganizationAppKey.HasValue ? payloadData.OrganizationAppKey.ToString() : string.Empty)
				}),
		    Expires = payloadData.ExpirationDate,
		    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(systemAuthSecretKey ?? string.Empty)), SecurityAlgorithms.HmacSha256Signature)
	    };

      var token = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
	    var barerToken = tokenHandler.WriteToken(token);

	    return new AuthResponseData
	    {
		    UserAppKey = userAppKey,
		    UserEmail = userLogin,
		    BarerToken = barerToken
      };
    }
  }
}